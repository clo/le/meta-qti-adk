SUMMARY = "A header-only dbus-binding leveraging C++-11"
SECTION = "examples"
LICENSE = "GPLv2 | LGPLv3"
DEPENDS = "boost process-cpp properties-cpp libxml2 dbus"
LIC_FILES_CHKSUM = "file://COPYING.GPL;md5=b234ee4d69f5fce4486a80fdaf4a4263 \
                    file://COPYING.LGPL;md5=e6a600fd5e1d9cbde2d983680233ad02"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "https://launchpad.net/ubuntu/+archive/primary/+files/${BPN}_${PV}+16.10.20160809.orig.tar.gz \
           file://01_fix_compile_error_and_disable_gtest.patch \
           file://01_fix_dbus-cpp_race.patch"

SRC_URI[md5sum] = "1ca6e981f4a53de0ca5dee6a314da7f6"
SRC_URI[sha256sum] = "2ed3b2c7a099e2820f4dc18aafd30452510f5e89745096a7ae9b77e57c3113b4"

S = "${WORKDIR}/"

inherit cmake pkgconfig
