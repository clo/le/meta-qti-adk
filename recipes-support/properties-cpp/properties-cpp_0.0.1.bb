SUMMARY = "A very simple convenience library for handling properties and signals in C++11"
SECTION = "examples"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=e6a600fd5e1d9cbde2d983680233ad02"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "https://launchpad.net/ubuntu/+archive/primary/+files/${BPN}_${PV}+14.10.20140730.orig.tar.gz \ 
           file://01_Temp_Fix.patch"

SRC_URI[md5sum] = "449f95cc864ebe38a35848885ca1cb4b"
SRC_URI[sha256sum] = "5c5400d5e7f16e90916d9be2742785c68e7ce544641739a380d2d815cff67223"

EXTRA_OECMAKE = "-DPROPERTIES_CPP_ENABLE_DOC_GENERATION=OFF"

S = "${WORKDIR}/${BP}+14.10.20140730"
RDEPEND_${PN} = ""

inherit cmake pkgconfig

