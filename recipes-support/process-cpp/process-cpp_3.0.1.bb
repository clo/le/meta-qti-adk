SUMMARY = "A simple convenience library for handling processes in C++11."
SECTION = "examples"
DEPENDS = "boost properties-cpp"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=e6a600fd5e1d9cbde2d983680233ad02"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "https://launchpad.net/ubuntu/+archive/primary/+files/${BPN}_${PV}.orig.tar.gz \
           file://01_disable_test.patch"

SRC_URI[md5sum] = "95e187de74037b70b105679f85047c12"
SRC_URI[sha256sum] = "33d13d384e99d3f57691c9c572c0c38fe4b67c056f19c8d99f18230f62b94a6b"

inherit cmake pkgconfig
