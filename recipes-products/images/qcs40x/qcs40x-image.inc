IMAGE_INSTALL += "packagegroup-adk"
IMAGE_INSTALL += "ca-certificates"

# Install tests and utils on non-production builds
IMAGE_INSTALL += "${@bb.utils.contains('VARIANT', 'user debug', '', 'packagegroup-adk-tests packagegroup-adk-utils', d)}"

# PulseAudio server is 32-bit only as qahw is 32-bit only
IMAGE_INSTALL += "${@bb.utils.contains('MULTILIB_VARIANTS', 'lib32', 'lib32-packagegroup-adk-pulseaudio', 'packagegroup-adk-pulseaudio', d)}"

# If we're multilib, add 32-bit gstreamer dependencies as well (in this case
# 64-bit comes from packagegroup-adk's dependencies)
IMAGE_INSTALL += "${@bb.utils.contains('MULTILIB_VARIANTS', 'lib32', 'lib32-packagegroup-adk-gstreamer', '', d)}"
IMAGE_INSTALL += "packagegroup-adk-gstreamer"

IMAGE_INSTALL_remove = "lib32-alexa-voice-client alexa-voice-client"

IMAGE_INSTALL += "${@bb.utils.contains('MULTILIB_VARIANTS', 'lib32', 'lib32-pulseaudio-effects', 'pulseaudio-effects', d)}"
