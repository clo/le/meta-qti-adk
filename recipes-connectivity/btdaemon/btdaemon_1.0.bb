inherit cmake pkgconfig systemd

DESCRIPTION = "Bluetooth Service Daemon"
LICENSE = "Apache-2.0"
HOMEPAGE = "https://www.codeaurora.org/"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/\
${LICENSE};md5=89aea4e17d99a7cacdbeed46a0096b10"

FILESPATH =+ "${WORKSPACE}:"
SRC_URI = " \
        file://vendor/qcom/opensource/bt-daemon/ \
        file://btdaemon.conf \
        file://btdaemon.service \
        "

S = "${WORKDIR}/vendor/qcom/opensource/bt-daemon/"

DEPENDS = "fluoride adk-utils libsystemdq bluetooth-le"
RDEPENDS_${PN} = "libsystemdq"

CPPFLAGS_append = " -DUSE_LIBHW_AOSP"

EXTRA_OECMAKE = "-DINCLUDES_SYSROOT=${STAGING_INCDIR}"

FILES_${PN} += " \
        ${systemd_system_unitdir} \
        ${sysconfdir} \
        "

do_install_append() {
        install -d ${D}${systemd_system_unitdir}
        install -m 0644 ${WORKDIR}/btdaemon.service ${D}${systemd_system_unitdir}

        install -d ${D}/${sysconfdir}/dbus-1/system.d/
        install -m 0644 ${WORKDIR}/btdaemon.conf ${D}${sysconfdir}/dbus-1/system.d/
}

SYSTEMD_SERVICE_${PN} = "btdaemon.service"
