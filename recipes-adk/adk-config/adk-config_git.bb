SUMMARY = "QSAP command line config tool project"
SECTION = "examples"
LICENSE = "BSD-3-Clause"
DEPENDS = "sqlite3 json-c"

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource/:"
SRC_URI = "file://adk-config/"
S = "${WORKDIR}/adk-config"

RDEPEND_${PN} = "sqlite3"

PACKAGES =+ "${PN}-lib"

FILES_${PN}-lib = "${libdir}/lib*.so*"
RRECOMMENDS_${PN}-lib = "${PN}"

inherit cmake pkgconfig
