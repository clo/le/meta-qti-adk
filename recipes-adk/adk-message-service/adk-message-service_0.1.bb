SUMMARY = "ADK Message Service Library"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

DEPENDS = "adk-ipc adk-messages xmlrpc-c"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESPATH_prepend = "${WORKSPACE}/:"

SRC_URI = "file://vendor/qcom/opensource/adk-message-lib/adk-message-service/ "

SRC_DIR = "${WORKSPACE}/vendor/qcom/opensource/adk-message-lib/adk-message-service/"
S = "${WORKDIR}/vendor/qcom/opensource/adk-message-lib/adk-message-service/"

RDEPENDS_${PN} = "dbus-lib glib-2.0 dbus-glib dbus-cpp adk-messages adk-ipc-lib xmlrpc-c"

PACKAGES =+ "${PN}-utils"

FILES_${PN}-utils = "${bindir}/*"

RRECOMMENDS_${PN} = "${PN}-utils"

FILES_SOLIBSDEV=""
SOLIBS="*.so"

inherit cmake pkgconfig systemd

