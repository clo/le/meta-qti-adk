SUMMARY = "QSAP IPC Library project"
SECTION = "examples"
LICENSE = "BSD-3-Clause"
DEPENDS = "glib-2.0 dbus-glib dbus-cpp"

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource/:"

SRC_URI = "file://adk-ipc/ \
           file://dbus-session-env.sh \
           file://dbus-launch.service \
           "

S = "${WORKDIR}/adk-ipc"

RDEPENDS_${PN} = "dbus-lib glib-2.0 dbus-glib dbus-cpp"

PACKAGES =+ "${PN}-lib"

FILES_${PN}-lib = "${libdir}/lib*.so*"
RRECOMMENDS_${PN}-lib = "${PN}"

EXTRA_OECMAKE = "-DWITH_IPC_EXAMPLES=ON \
                 -DWITH_TESTS=OFF \
                "

FILES_${PN}-dbg_append = " \
  ${libdir}/${BPN}/examples/rules-manager/.debug \
  ${libdir}/${BPN}/examples/led-manager/.debug \
  ${libdir}/${BPN}/examples/button-manager/.debug"

inherit cmake pkgconfig systemd

do_install_append() {
  install -d -m 0755 ${D}${bindir}/
  install -m 0755 ${WORKDIR}/dbus-session-env.sh ${D}${bindir}/

  install -d ${D}${systemd_system_unitdir}
  install -m 0644 ${WORKDIR}/dbus-launch.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "dbus-launch.service"
