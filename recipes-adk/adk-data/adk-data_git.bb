SUMMARY = "ADK Config Data"
LICENSE = "BSD-3-Clause"

FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource/:"

SRC_URI = "file://adk-data/"

FILES_${PN} += "${userfsdatadir}/adk.rules.db \
                ${userfsdatadir}/adk.core.db \
                ${userfsdatadir}/adk.button.db \
                ${userfsdatadir}/adk.device.db \
                ${userfsdatadir}/adk.connectivity.wifi.db \
                ${userfsdatadir}/adk.connectivity.bt.db \
                ${userfsdatadir}/adk.connectivity.states.db \
                ${userfsdatadir}/adk.led.db \
                ${userfsdatadir}/allplay.db \
                ${userfsdatadir}/spotify.db \
                ${userfsdatadir}/iotsys.db \
               "
inherit allarch

do_install() {
  install -d ${D}${userfsdatadir}
  install -m 0644 ${WORKDIR}/adk-data/adk.device.db ${D}${userfsdatadir}/adk.device.db
  install -m 0644 ${WORKDIR}/adk-data/adk.rules.db ${D}${userfsdatadir}/adk.rules.db
  install -m 0644 ${WORKDIR}/adk-data/adk.core.db ${D}${userfsdatadir}/adk.core.db
  install -m 0644 ${WORKDIR}/adk-data/adk.button.db ${D}${userfsdatadir}/adk.button.db
  install -m 0644 ${WORKDIR}/adk-data/adk.connectivity.wifi.db ${D}${userfsdatadir}/adk.connectivity.wifi.db
  install -m 0644 ${WORKDIR}/adk-data/adk.connectivity.bt.db ${D}${userfsdatadir}/adk.connectivity.bt.db
  install -m 0644 ${WORKDIR}/adk-data/adk.connectivity.states.db ${D}${userfsdatadir}/adk.connectivity.states.db
  install -m 0644 ${WORKDIR}/adk-data/adk.led.db ${D}${userfsdatadir}/adk.led.db
  install -m 0644 ${WORKDIR}/adk-data/allplay.db ${D}${userfsdatadir}/allplay.db
  install -m 0644 ${WORKDIR}/adk-data/spotify.db ${D}${userfsdatadir}/spotify.db
  install -m 0644 ${WORKDIR}/adk-data/iotsys.db ${D}${userfsdatadir}/iotsys.db
}
