SUMMARY = "IoTSys App"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource:"
SRC_URI = "\
    file://adk-iot-sys/ \
    file://iotsys.service \
    "

SRC_DIR = "${WORKSPACE}/vendor/qcom/opensource/adk-iot-sys/"
S = "${WORKDIR}/adk-iot-sys/"

DEPENDS = "adk-utils adk-message-service adk-config iotivity gtest"
RDEPENDS_${PN} += "iotivity adk-managers adk-message-service"

EXTRA_OECMAKE += "\
    -DDEVICE_CONFIG_FILENAME=${userfsdatadir}/adk.device.db \
    -DVOICEUI_CONFIG_FILENAME=${userfsdatadir}/adk.voiceui.db \
    "

inherit cmake systemd

do_install_append() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/iotsys.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = " \
    iotsys.service \
    "
