SUMMARY = "ADK software managers project"
SECTION = "base"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"
DEPENDS = "adk-utils adk-config adk-ipc data dsutils qmi qmi-framework libnl pulseaudio adk-mediaplayer-lib pa-qti-effect sqlite3-native gtest pa-qti-loopback boost libsystemdq"
# The following dependencies are to fix the compilation when using the prebuilt
# version of "data" (missing header, missing libraries), likely caused by a bug
# in the way tasks dependencies are computed by bitbake when using a prebuilt
# package, like its dependencies are not correctly inherited/computed.
DEPENDS += "common configdb xmllib diag time-genoff adk-messages adk-message-service quartz-host liblog bluetooth-le"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource/:"

SRC_URI = "file://adk-managers/ \
           file://connectivity-manager.service \
           file://button-manager.service \
           file://led-manager.service \
           file://rules-manager.service \
           file://audio-manager.service \
           file://hdmi-player.service \
           file://spdif-player.service \
	   file://linein-player.service \
           file://audio-manager.conf \
           file://hdmi-port-enable.service \
           file://system-manager.service \
           "

S = "${WORKDIR}/adk-managers"

RDEPENDS_${PN} = "data dsutils qmi qmi-framework libnl lighttpd lighttpd-module-rewrite pulseaudio adk-mediaplayer-lib adk-messages adk-message-service bluetooth-le"

# BT-daemon is added to RDEPENDS if BT control feature is enabled in Connectivity Manager
CONNECTIVITY_MANAGER_BT_CONTROL_ENABLED ??= "ON"
RDEPENDS_${PN} += "${@ 'btdaemon' if d.getVar('CONNECTIVITY_MANAGER_BT_CONTROL_ENABLED') == "ON" else ''}"

CONNECTIVITY_MANAGER_ENABLE_ZIGBEE_CONTROL ??= "ON"

BUTTON_MANAGER_CONFIG_BUTTON_DB ??= "/data/adk.button.db"
RULES_MANAGER_CONFIG_RULES_DB   ??= "/data/adk.rules.db"
AUDIO_MANAGER_CONFIG_AUDIO_DB ??= "/data/adk.audiomanager.db"
LED_MANAGER_CONFIG_LED_DB ??= "/data/adk.led.db"
CONNECTIVITY_MANAGER_CONFIG_DEVICE_DB ??= "/data/adk.device.db"
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_STATES_DB ??= "/data/adk.connectivity.states.db"
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_WIFI_DB ??= "/data/adk.connectivity.wifi.db"
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_BT_DB ??= "/data/adk.connectivity.bt.db"
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_ZIGBEE_DB ??= "/data/adk.connectivity.zigbee.db" 
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_COEX_DB ??= "/data/adk.connectivity.coex.db"
CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_ETHERNET_DB ??= "/data/adk.connectivity.ethernet.db"


# This number is taken from sysexits for EX_CONFIG
RESTART_PREVENT_EXIT_CODE ??= "78"
# systemd restart delay
RESTART_DELAY ??= "5"

RDEPENDS_${PN} += "${@ 'quartz-host quartz-host-lib' if d.getVar('CONNECTIVITY_MANAGER_ENABLE_ZIGBEE_CONTROL') == "ON" else ''}"

EXTRA_OECMAKE = " \
                 -DCONFIG_SYSTEM_DB="/data/adk.system.db" \
                 -DCONFIG_AUDIO_DB="/data/adk.audiomanager.db" \
                 -DCONFIG_CORE_DB="/data/adk.core.db" \
                 -DCONFIG_RULES_DB="${RULES_MANAGER_CONFIG_RULES_DB}" \
                 -DCONFIG_AUDIO_DB="${AUDIO_MANAGER_CONFIG_AUDIO_DB}" \
                 -DCONFIG_BUTTON_DB="${BUTTON_MANAGER_CONFIG_BUTTON_DB}" \
                 -DCONFIG_CONNECTIVITY_WIFI_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_WIFI_DB}" \
                 -DCONFIG_CONNECTIVITY_BT_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_BT_DB}" \
                 -DCONFIG_CONNECTIVITY_STATES_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_STATES_DB}" \
                 -DCONFIG_CONNECTIVITY_ZIGBEE_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_ZIGBEE_DB}" \
                 -DCONFIG_CONNECTIVITY_COEX_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_COEX_DB}" \
                 -DCONFIG_CONNECTIVITY_ETHERNET_DB="${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_ETHERNET_DB}" \
                 -DCONFIG_DEVICE_DB="${CONNECTIVITY_MANAGER_CONFIG_DEVICE_DB}" \
                 -DCONFIG_LED_DB="${LED_MANAGER_CONFIG_LED_DB}" \
                 -DWITH_CONNECTIVITY_MANAGER=ON \
                 -DWITH_CONNECTIVITY_MANAGER_BT_CONTROL=${CONNECTIVITY_MANAGER_BT_CONTROL_ENABLED} \
                 -DWITH_CONNECTIVITY_MANAGER_ZIGBEE_CONTROL=${CONNECTIVITY_MANAGER_ENABLE_ZIGBEE_CONTROL} \
                 -DWITH_TESTS=OFF \
                 ${@bb.utils.contains('BASEMACHINE', 'qcs40x', '-DBASEMACHINE=${BASEMACHINE}', '', d)} \
                "
TARGET_CPPFLAGS += " \
                    -I${STAGING_INCDIR}/dsutils \
                    -I${STAGING_INCDIR}/qmi \
                    -I${STAGING_INCDIR}/qmi-framework \
                    -I${STAGING_INCDIR}/libnl3 \
                   "

inherit cmake pkgconfig systemd

do_install_append() {
    install -d ${D}${base_sbindir}
    install -m 0755 ${S}/connectivity-manager/src/wifi-control/adk_wifi_config_update ${D}${base_sbindir}
    install -m 0755 ${S}/connectivity-manager/src/wifi-control/adk_wifi_wpa_supplicant_update ${D}${base_sbindir}

    install -d ${D}${systemd_system_unitdir}
    # Install and process the connectivity-manager service file.
    install -m 0644 ${WORKDIR}/connectivity-manager.service ${D}${systemd_system_unitdir}
    sed -i -e 's,@CONFIG_DEVICE_DB@,${CONNECTIVITY_MANAGER_CONFIG_DEVICE_DB},g'  \
           -e 's,@CONFIG_CONNECTIVITY_STATES_DB@,${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_STATES_DB},g'  \
           -e 's,@CONFIG_CONNECTIVITY_WIFI_DB@,${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_WIFI_DB},g'  ${D}${systemd_system_unitdir}/connectivity-manager.service
    
    if [ "${CONNECTIVITY_MANAGER_BT_CONTROL_ENABLED}" == "ON" ]; then
        sed -i -e 's,@CONFIG_CONNECTIVITY_BT_DB@,${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_BT_DB},g'  ${D}${systemd_system_unitdir}/connectivity-manager.service
    else
        # if BT is not enabled remove all lines that require it from the service file
        sed -i -e '/@CONFIG_CONNECTIVITY_BT_DB@/d' \
               -e '/btdaemon.service/d'  ${D}${systemd_system_unitdir}/connectivity-manager.service
    fi
    if [ "${CONNECTIVITY_MANAGER_ENABLE_ZIGBEE_CONTROL}" == "ON" ]; then
        sed -i -e 's,@CONFIG_CONNECTIVITY_ZIGBEE_DB@,${CONNECTIVITY_MANAGER_CONFIG_CONNECTIVITY_ZIGBEE_DB},g'  ${D}${systemd_system_unitdir}/connectivity-manager.service
    else
        sed -i '/@CONFIG_CONNECTIVITY_ZIGBEE_DB@/d'  ${D}${systemd_system_unitdir}/connectivity-manager.service
    fi

    # Install and process the button-manager service file.
    install -m 0644 ${WORKDIR}/button-manager.service ${D}${systemd_system_unitdir}
    sed -i -e 's,@CONFIG_BUTTON_DB@,${BUTTON_MANAGER_CONFIG_BUTTON_DB},g'  ${D}${systemd_system_unitdir}/button-manager.service

    # Install and process the led-manager service file.
    install -m 0644 ${WORKDIR}/led-manager.service ${D}${systemd_system_unitdir}
    sed -i -e 's,@CONFIG_LED_DB@,${LED_MANAGER_CONFIG_LED_DB},g'  ${D}${systemd_system_unitdir}/led-manager.service

    # Install and process the rules manager service file
    install -m 0644 ${WORKDIR}/rules-manager.service ${D}${systemd_system_unitdir}
    sed -i -e 's,@CONFIG_RULES_DB@,${RULES_MANAGER_CONFIG_RULES_DB},g'  ${D}${systemd_system_unitdir}/rules-manager.service

    # Install the audio manager service file
    install -m 0644 ${WORKDIR}/audio-manager.service ${D}${systemd_system_unitdir}
    sed -i -e 's,@CONFIG_AUDIO_DB@,${AUDIO_MANAGER_CONFIG_AUDIO_DB},g'  ${D}${systemd_system_unitdir}/audio-manager.service

    install -m 0644 ${WORKDIR}/hdmi-player.service ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/spdif-player.service ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/linein-player.service ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/hdmi-port-enable.service ${D}${systemd_system_unitdir}

    install -d ${D}/${sysconfdir}/dbus-1/system.d/
    install -m 0644 ${WORKDIR}/audio-manager.conf ${D}${sysconfdir}/dbus-1/system.d/
    install -m 0644 ${WORKDIR}/system-manager.service ${D}${systemd_system_unitdir}
    
    sed -i -e 's,@BINDIR@,${bindir},g'  \
           -e 's,@RESTART_PREVENT@,${RESTART_PREVENT_EXIT_CODE},g'  \
           -e 's,@RESTART_DELAY@,${RESTART_DELAY},g'  ${D}${systemd_system_unitdir}/*.service

}

SYSTEMD_SERVICE_${PN} = " \
                         connectivity-manager.service \
                         button-manager.service \
                         led-manager.service \
                         rules-manager.service \
                         audio-manager.service \
                         hdmi-player.service \
                         spdif-player.service \
                         linein-player.service \
                         hdmi-port-enable.service \
                         system-manager.service \
                        "

# display-manager is optional on some targets.
include ${BASEMACHINE}/${MACHINE}-display-manager.inc
