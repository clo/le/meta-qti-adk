# display manager specific items
# wpewebkit - core web browser libraries, cog - web browser implementation
RDEPENDS_${PN} += " wpewebkit cog"

SRC_URI_append = " file://display-bridge.service \
                  file://display-browser.service \
                  file://web/ \
                "

do_install_append() {
    install -d ${D}${systemd_system_unitdir}

    install -m 0644 ${WORKDIR}/display-browser.service ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/display-bridge.service ${D}${systemd_system_unitdir}

    sed -i -e 's,@BINDIR@,${bindir},g'  \
           -e 's,@RESTART_PREVENT@,${RESTART_PREVENT_EXIT_CODE},g'  \
           -e 's,@RESTART_DELAY@,${RESTART_DELAY},g' ${D}${systemd_system_unitdir}/*.service

    install -d ${D}${userfsdatadir}/display-manager
    cp -a ${WORKDIR}/web/* ${D}${userfsdatadir}/display-manager/
}

FILES_${PN} += "${userfsdatadir}/display-manager"
SYSTEMD_SERVICE_${PN} += "display-browser.service display-bridge.service"
