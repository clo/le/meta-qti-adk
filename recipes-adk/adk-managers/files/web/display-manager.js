/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

var debug_bar_array = [];
var AdkMessageSubscriptionList = [
        "group=allplay;" +
        "group=audio;" +
        "group=connectivity;" +
        "group=voiceui;" +
        "group=led;" +
        "group=button;" +
        "group=display;" +
        "group=notification;" +
        "group=iotsys;"];

var AdkMessageSourceAddress = "http://127.0.0.1:9000"
var AdkMessageSource;

/*
 * Subscribe to the messages when the DOM has loaded
 */
document.addEventListener("DOMContentLoaded", 
    function() {
        console.log("start the ADK Message Server");
        StartAdkMessageServer(AdkMessageSubscriptionList);
    }
);
var counter = 0;

/* TESTING
setInterval(function(){ 
        counter++;
        HandleAdkMessageMsg( {data:'{"display_device_name":{"name":"' + counter + '"}}'} );
    }, 500 );
*/

/*
 * Debug Bar Handler
 */
 function UpdateDebugBar(new_message){
    var debug_bar = document.getElementById("debug_bar");

    var length = debug_bar_array.unshift(new_message);
    if ( length > 40 )
        debug_bar_array.pop();

    debug_bar.innerHTML = "";
    debug_bar_array.forEach(function(item,index){ debug_bar.innerHTML += item + '<br>';});    
}

/*
 * Subscribe to messages by starting the ADK message server which will forward on
 * ADK messages to this Javascript. The subscription requires passing a 
 * list of messages that the server will subscribe to using the ADK Message
 * API.
 */
function StartAdkMessageServer(sub_list){
    /* option to use a local server or a CGI server */
    /*  AdkMessageSource = new EventSource("display-bridge.cgi?"+sub_list); */
    AdkMessageSource = new EventSource(AdkMessageSourceAddress+"?"+sub_list);
    AdkMessageSource.addEventListener("info", HandleAdkMessageInfo, false);
    AdkMessageSource.addEventListener("msg", HandleAdkMessageMsg, false);
    AdkMessageSource.onerror = function(error){ UpdateDebugBar("error: could not connect to display-bridge"); }
}

/*
 * Disconnect from the ADK message Server
 */
function StopAdkMessageServer(){
    console.log("closing AdkMessageSource");
    if (AdkMessageSource!=null) {
        AdkMessageSource.close();
        if (AdkMessageSource.readyState == 2) {
            console.log ("AdkMessageSource closed");
        }
    }
}

/*
 * Side channel information. String only.
 */
function HandleAdkMessageInfo(event) {
    console.log("ADK message server: " + event.data);
    UpdateDebugBar( "info:" + event.data );
}


/*
 * Central dispatcher for ADK Messages
 */
function HandleAdkMessageMsg(event){
    var adkmsg = JSON.parse(event.data);
    var message_name = Object.keys(adkmsg)[0];
    var payload = adkmsg[message_name];

    UpdateDebugBar("data:" + message_name + ":" + JSON.stringify(payload) );
}

