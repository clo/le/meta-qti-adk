FILESEXTRAPATHS_prepend := "${THISDIR}/quartz_host_patch:"
SRC_URI_append = " \
  file://quartz-host.patch \ 
"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

do_patch() {
    if [ -f ${WORKDIR}/0000-platform.patch ]; then
        cd ${S}
        patch -p2 < ${WORKDIR}/0000-platform.patch
    fi;
    cd ${S}
    patch -p2 < ${WORKDIR}/quartz-host.patch
}

SRC_URI += "file://iotd.service \
"

do_compile_append() {
    make CC="${CC}" HOST_CROSS_COMPILE=other -C ${S}/exthost/Linux/qapi
    make CC="${CC}" HOST_CROSS_COMPILE=other -C ${S}/exthost/Linux/daemon
    make CC="${CC}" HOST_CROSS_COMPILE=other -C ${S}/exthost/Linux/app/NB_QCLI_demo/build
    make CC="${CC}" HOST_CROSS_COMPILE=other -C ${S}/exthost/Linux/app/fwupgrade
}

PACKAGES = "${PN}"

PACKAGES =+ "${PN}-lib"
PACKAGES =+ "${PN}-dbg"


FILES_${PN}-lib = "${libdir}/lib*.so*"
FILES_${PN}-dbg += "${bindir}/.debug"

RRECOMMENDS_${PN}-lib = "${PN}"
RRECOMMENDS_${PN}-dbg = "${PN}"


inherit cmake pkgconfig systemd

do_install_append(){

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/iotd.service ${D}${systemd_system_unitdir}
    
    install -m 0644 ${WORKDIR}/iotd_config.ini -D ${D}${sysconfdir}/iotd_config.ini
    install -d ${D}${includedir}/quartz_host/

    install -d ${D}${bindir}
    install -m 0755 ${S}/exthost/Linux/app/NB_QCLI_demo/build/nb_demo -D ${D}${bindir}/
    install -m 0755 ${S}/exthost/Linux/daemon/output/iotd -D ${D}${bindir}/
    install -m 0755 ${S}/exthost/Linux/app/fwupgrade/qca_mgr_daemon -D ${D}${bindir}/
}

SYSTEMD_SERVICE_${PN} = " \
                         iotd.service \
"
