DESCRIPTION = "Adk System Interface sample application"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

PR = "r1"
# Sample Application Directory
SAMPLE_APP_DIR = "code-sample"
# Local full-path of sample application code
SAMPLE_APP_PATH_PREPEND = "${WORKSPACE}/vendor/qcom/opensource/adk-tools/adk-system-interface-tool"

FILESPATH_prepend = "${SAMPLE_APP_PATH_PREPEND}:"
SRC_URI = "file://${SAMPLE_APP_DIR}"

SRC_DIR = "${SAMPLE_APP_PATH_PREPEND}/${SAMPLE_APP_DIR}"
S = "${WORKDIR}/${SAMPLE_APP_DIR}"

DEPENDS = "adk-messages adk-message-service glib-2.0"
RDEPENDS_${PN} = "adk-messages adk-message-service glib-2.0"

inherit cmake
