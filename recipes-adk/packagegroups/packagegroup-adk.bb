SUMMARY = "ADK package groups"

PR="r1"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PACKAGES = " \
    packagegroup-adk \
    packagegroup-adk-tests \
    packagegroup-adk-utils \
    "

RDEPENDS_packagegroup-adk = " \
    adk-managers \
    voice-ui-framework \
    iotsys \
    adk-system-interface-sampleapp \
    "

RDEPENDS_packagegroup-adk-tests = " \
    adk-mediaplayer-lib-tests \
    "

RDEPENDS_packagegroup-adk-utils = " \
    adk-mediaplayer-lib-cli \
    pulseaudio-misc \
    adk-message-service-utils \
    "

