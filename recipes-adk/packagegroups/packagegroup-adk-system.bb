SUMMARY = "ADK package groups (system)"

PR="r1"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

PACKAGES = " \
    packagegroup-adk-gstreamer \
    packagegroup-adk-pulseaudio \
    "

# For multilib systems, we pick up 32- and 64-bit versions of these packages
RDEPENDS_packagegroup-adk-gstreamer = " \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly-asf \
    gstreamer1.0-libav \
    "

# PulseAudio server + plugins we want to ship
RDEPENDS_packagegroup-adk-pulseaudio = " \
    pulseaudio-server \
    pulseaudio-module-bluetooth-policy \
    pulseaudio-module-bluetooth-discover \
    pulseaudio-module-bluez5-discover \
    pulseaudio-module-bluez5-device \
    pulseaudio-module-loopback \
    pulseaudio-module-null-source \
    pulseaudio-module-combine-sink \
    pulseaudio-module-role-exclusive \
    pulseaudio-module-role-ignore \
    pulseaudio-module-role-ducking \
    pulseaudio-module-policy-voiceui \
    pulseaudio-module-remap-sink \
    "
