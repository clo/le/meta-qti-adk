SUMMARY = "ADK Messages Library"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

DEPENDS = "protobuf protobuf-native"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESPATH_prepend = "${WORKSPACE}/:"

SRC_URI = "file://vendor/qcom/opensource/adk-message-lib/adk-messages/ "

SRC_DIR = "${WORKSPACE}/vendor/qcom/opensource/adk-message-lib/adk-messages"
S = "${WORKDIR}/vendor/qcom/opensource/adk-message-lib/adk-messages/"

RDEPENDS_${PN} = "protobuf"

SOLIBS = ".so"
FILES_SOLIBSDEV = ""

inherit cmake pkgconfig systemd

