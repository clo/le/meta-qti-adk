SUMMARY = "ADK modules on-target test project"
SECTION = "base"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"
DEPENDS = "gtest adk-ipc adk-message-service adk-messages"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESPATH_prepend = "${WORKSPACE}/:"

SRC_URI = "file://vendor/qcom/opensource/adk-managers/target-test/"

SRC_DIR = "${WORKSPACE}/vendor/qcom/opensource/adk-managers/target-test"
S = "${WORKDIR}/vendor/qcom/opensource/adk-managers/target-test"

RDEPENDS_${PN} = "gtest adk-message-service adk-messages"

INSANE_SKIP_${PN} += "dev-deps"

inherit cmake pkgconfig
