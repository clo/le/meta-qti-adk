DESCRIPTION = "QTI Voice UI Framework"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"


PR = "r25"
WORKSPACE_VUIFWORKDIR = "adk-voice-ui-framework"

FILESPATH_prepend = "${WORKSPACE}/vendor/qcom/opensource:"

SRC_URI = "\
            file://${WORKSPACE_VUIFWORKDIR} \
            file://voiceUI.service \
            file://voiceUI.path \
            file://sdk_setup \
            file://schema/voiceui-schema-db.txt \
          "

inherit cmake systemd siteinfo

#By Default AVS is the SDK supported by voice UI Framework
#To add support for other SDKs change your local.conf or add to distro configuration
#e.g. to add cortana add PACKAGECONFIG_append_pn-voice-ui-framework = " cortana"

PACKAGECONFIG ??= "avs modular-client"

#Syntax : PACKAGECONFIG[f1] = "--with-f1,--without-f1,build-deps-f1,rt-deps-f1"
PACKAGECONFIG[avs] = ",,avs-device-sdk,avs-device-sdk ( >= 1.13)"
PACKAGECONFIG[modular-client] = ",,qti-local-asr,qti-local-asr-staticdev"
PACKAGECONFIG[cortana] = ",,,"
PACKAGECONFIG[googleassistant] = ",,,"
PACKAGECONFIG[baidu] = ",,,"
#Add more package configs to include other dependent SDKs


#Add dependencies based on platform
#TBD for Vipertooth
DEPENDS += "${@bb.utils.contains('MACHINE', 'apq8009', 'curl adk-config adk-ipc jsoncpp gstreamer1.0 gstreamer1.0-plugins-base pa-qti-soundtrigger', '', d)} \
            ${@bb.utils.contains('MACHINE', 'apq8017', 'curl adk-config adk-ipc jsoncpp gstreamer1.0 gstreamer1.0-plugins-base pa-qti-soundtrigger', '', d)} \
            ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'curl adk-config adk-ipc jsoncpp gstreamer1.0 gstreamer1.0-plugins-base pa-qti-soundtrigger system-media adk-mediaplayer-lib adk-messages adk-message-service adk-utils', '', d)} \
            ${@bb.utils.contains('PACKAGECONFIG', 'avs modular-client', 'gtest', '', d)} \
           "

RDEPENDS_${PN} += "${@bb.utils.contains('MACHINE', 'apq8009', 'jsoncpp', '', d)} \
                   ${@bb.utils.contains('MACHINE', 'apq8017', 'jsoncpp', '', d)} \
                   ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'jsoncpp', '', d)} \
                   ${@bb.utils.contains('PACKAGECONFIG', 'avs modular-client', 'gtest', '', d)} \
                  "
#RDEPENDS_${PN}-tests = "${PN}"

SRC_DIR = "${WORKSPACE}/vendor/qcom/proprietary/${WORKSPACE_VUIFWORKDIR}/"
S = "${WORKDIR}/${WORKSPACE_VUIFWORKDIR}/"


# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
# TODO: Separate UnitTests as per definition of BUILD_MODULAR_CLIENT and BUILD_AVS
EXTRA_OECMAKE = " \
                  ${@bb.utils.contains('PACKAGECONFIG', 'avs modular-client', '', '', d)} \
                  ${@bb.utils.contains('PACKAGECONFIG', 'avs', '-DBUILD_AVS=1', '', d)} \
                  ${@bb.utils.contains('PACKAGECONFIG', 'modular-client', '-DBUILD_MODULAR_CLIENT=1', '', d)} \
                  -DSYSROOT_DIR="${STAGING_DIR_TARGET}" \
                  -DCONFIG_VOICEUI_DB_PATH=${userfsdatadir}/adk.voiceui.db \
                  -DCONFIG_AUDIOMANAGER_DB_PATH=${userfsdatadir}/adk.audiomanager.db \
                "

EXTRA_OECMAKE += "-DGSTREAMER_MEDIA_PLAYER=ON \
 -DSYSROOT_DIR=${PKG_CONFIG_SYSROOT_DIR} \
 -DLIB_PREFIX=${SITEINFO_BITS}"

python __anonymous() {
	ws = d.getVar('WORKSPACE', True)
	bits = d.getVar('SITEINFO_BITS', True)

 	# Include AVS_WWE FLAG only if AVS WWE lib and .h fle is present (lib must be obtained from Amazon)
	header_path_avs_wwe = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/Wakeword/include/Wakeword/AVS_WWE/pryon_lite.h'
	
	rd = d.getVar('EXTRA_OECMAKE', True)
	if os.path.isfile(header_path_avs_wwe):
		lib_path_avs_wwe = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/Wakeword/lib/avs'+bits+'/libpryon_lite.so.2.2'
		if os.path.isfile(lib_path_avs_wwe):
			d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_AVS_WWE=1" )
		else:
			d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_AVS_WWE=0" )
	else:
		d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_AVS_WWE=0" )

	# Include DSPC FLAG only if DSPC lib and headers are present (headers and lib must be obtained from DSPC)
	lib_path_dspc = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/AudioRecorder/lib/dspc'+bits+'/'
	dspc_lib_exist = os.path.isfile(lib_path_dspc+'AWELib.so') and os.path.isfile(lib_path_dspc+'UtilLib.a')

	header_path_dspc = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/AudioRecorder/include/AudioRecorder/DSPC/'
	dspc_headers = ["TcpIO2.h", "awelib.h", "basehelpers.h","utiltypes.h"]
	dspc_headers_exist = True
	for hfile in dspc_headers:
		hpath = header_path_dspc + hfile
		if os.path.isfile(hpath):
			continue
		else:
			dspc_headers_exist = False
			break
	rd = d.getVar('EXTRA_OECMAKE', True)
	if dspc_lib_exist and dspc_headers_exist:
		d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_DSPC_CLIENT=1" )
	else:
		d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_DSPC_CLIENT=0" )
	
	# Include SENSORY FLAG only if SENSORY lib is present (lib must be obtained from SENSORY)
	header_path_sensory = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/Wakeword/include/Wakeword/Sensory/snsr.h'

	rd = d.getVar('EXTRA_OECMAKE', True)
	if os.path.isfile(header_path_sensory):
		lib_path_sensory = ws + '/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/Wakeword/lib/sensory'+bits+'/libsnsr.a'
		if os.path.isfile(lib_path_sensory):
			d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_SENSORY_CLIENT=1" )
		else:
			d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_SENSORY_CLIENT=0" )
	else:
		d.setVar('EXTRA_OECMAKE', rd + " -DBUILD_SENSORY_CLIENT=0" )
		
}

#Allow packaging unversioned libraries 
#TODO: Version
SOLIBS = ".so"
FILES_SOLIBSDEV = ""

#Specify install appends
do_install_append() {

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/voiceUI.service -D ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/voiceUI.path -D ${D}${systemd_system_unitdir}

    install -d ${D}${userfsdatadir}/voice-ui-framework
    install -d ${D}${datadir}/sounds/vipertooth
    #modular-client Setup Files
    if ${@bb.utils.contains('PACKAGECONFIG', 'modular-client', 'true', 'false', d)}; then
        install -m 0644 ${WORKDIR}/sdk_setup/modular/asr-nlu/qualcomm/HeySnapdragon.uim ${D}${userfsdatadir}/voice-ui-framework
        install -m 0644 ${WORKDIR}/sdk_setup/modular/asr-nlu/qualcomm/model.float.1.0_1.3.0.asr ${D}${userfsdatadir}/voice-ui-framework
        install -m 0644 ${WORKDIR}/sdk_setup/modular/asr-nlu/qualcomm/canned_mp3_files/*.mp3 ${D}${userfsdatadir}/voice-ui-framework
    fi
    #AVS Setup Files
    if ${@bb.utils.contains('PACKAGECONFIG', 'avs', 'true', 'false', d)}; then
		#If present copy DSPC libs to lib output dir
		if ${@bb.utils.contains('EXTRA_OECMAKE', '-DBUILD_AVS_WWE=1', 'true', 'false', d)}; then
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/alexa_gmm_VT_up40.uim ${D}${userfsdatadir}/voice-ui-framework
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/voice_prompts/Alexa_*.mp3 ${D}${datadir}/sounds/vipertooth
 		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/sm3_gmm_0703_cnn_jan08_Alexa.uim ${D}${userfsdatadir}/voice-ui-framework
		    install -m 0644 ${WORKDIR}/adk-voice-ui-framework/VoiceUIFramework/Wakeword/lib/avs${SITEINFO_BITS}/libpryon_lite.so.2.2 ${D}/${libdir}
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/WWE${SITEINFO_BITS}/D.en-US.alexa.bin ${D}${userfsdatadir}/voice-ui-framework
		fi
		if ${@bb.utils.contains('EXTRA_OECMAKE', '-DBUILD_AVS_WWE=0', 'true', 'false', d)}; then
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/alexa_gmm_VT_up40.uim ${D}${userfsdatadir}/voice-ui-framework
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/voice_prompts/Alexa_*.mp3 ${D}${datadir}/sounds/vipertooth
		    install -m 0644 ${WORKDIR}/sdk_setup/full/avs/sm3_gmm_0703_cnn_jan08_Alexa.uim ${D}${userfsdatadir}/voice-ui-framework
		fi
    fi

	#If present copy DSPC libs to lib output dir
	if ${@bb.utils.contains('EXTRA_OECMAKE', '-DBUILD_DSPC_CLIENT=1', 'true', 'false', d)}; then
		install -m 0644 ${WORKSPACE}/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/AudioRecorder/lib/dspc${SITEINFO_BITS}/AWELib.so ${D}/${libdir}
		install -m 0644 ${WORKSPACE}/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/AudioRecorder/lib/dspc${SITEINFO_BITS}/UtilLib.a ${D}/${libdir}
		install -m 0644 ${WORKDIR}/sdk_setup/3Party/dspc${SITEINFO_BITS}/qc-dspc-engine.awb ${D}${userfsdatadir}/voice-ui-framework
    fi

	#If present copy Sensory libs to lib output dir
	if ${@bb.utils.contains('EXTRA_OECMAKE', '-DBUILD_SENSORY_CLIENT=1', 'true', 'false', d)}; then
        install -m 0644 ${WORKSPACE}/vendor/qcom/opensource/adk-voice-ui-framework/VoiceUIFramework/Wakeword/lib/sensory${SITEINFO_BITS}/libsnsr.a ${D}/${libdir}/
        install -m 0644 ${WORKDIR}/sdk_setup/3Party/sensory${SITEINFO_BITS}/thfft_alexa_enus_THF6.0_1mbi.snsr ${D}${userfsdatadir}/voice-ui-framework
    fi
}

#FILES_${PN}-tests = "\
#    ${bindir}/*Test* \
#    "

FILES_${PN} += " \
    ${systemd_system_unitdir} \
    ${userfsdatadir} \
    ${datadir} \
    ${libdir} \
    "

SYSTEMD_SERVICE_${PN} = "voiceUI.service voiceUI.path"

#PACKAGES =+ "${PN}-tests"
