DESCRIPTION = "MediaPlayer Library"
DEPENDS = "gstreamer1.0 \
           gstreamer1.0-plugins-base \
           glib-2.0 \
           libsystemdq \
           adk-config \
           audio-qap-wrapper \
           configdb \
           gtest"
DEPENDS_append_qcs40x = " audio-melod-gst-noship"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

FILESPATH = "${WORKSPACE}/vendor/qcom/opensource:"

SRC_URI = "file://${BPN}"

S = "${WORKDIR}/${BPN}"
EXTRA_OECMAKE = " \
                 -DDATA_DIR="/usr/share/${BPN}/media" \
                 -DCONFIG_MEDIAPLAYER_DB="/data/adk.mediaplayer.db" \
                 ${@bb.utils.contains('BASEMACHINE', 'qcs40x', '-DBASEMACHINE=${BASEMACHINE}', '', d)} \
                "

inherit cmake pkgconfig

SOLIBS = ".so"
FILES_SOLIBSDEV = ""

FILES_${PN}-tests = "\
        ${bindir}/mediaplayer-test"

FILES_${PN}-data = "\
        /usr/share/${BPN}/media/audio.ogg \
        /usr/share/${BPN}/media/audio.m4a \
        /usr/share/${BPN}/media/image.png"

FILES_${PN}-cli = "\
        ${bindir}/mediaplayer-cli"

RDEPENDS_${PN} = "gstreamer1.0-plugins-base-playback \
                  gstreamer1.0-plugins-base-app \
                  libsystemdq"
RDEPENDS_${PN}-tests = "${PN}-data"

PACKAGES =+ "${PN}-tests ${PN}-data ${PN}-cli"
