inherit cmake systemd

SUMMARY = "PulseaudioEffects"
LICENSE = "LGPL-2.1"
LIC_FILES_CHKSUM = "file://${WORKSPACE}/external/pulseaudio/src/modules/filter-module/src/module-filter-sink.c;endline=21;md5=5238a62243554deed3f9e0ebb69d8e09 \
                    file://${WORKSPACE}/external/pulseaudio/src/modules/filter-module/src/inc/module-filter-sink.h;endline=21;md5=5238a62243554deed3f9e0ebb69d8e09 \
                   "

FILESPATH_prepend = "${WORKSPACE}/external/pulseaudio/src/modules:"
SRC_URI = "\
    file://filter-module/ \
    "

SRC_DIR = "${WORKSPACE}/external/pulseaudio/src/modules/filter-module/"
S = "${WORKDIR}/filter-module/"

DEPENDS = "adk-ipc pulseaudio glib-2.0 gstreamer1.0 libcgroup gstreamer1.0-plugins-base"


do_install_append() {
    install -d ${D}/${libdir}/pulse-12.2/modules/
    install ${D}/${libdir}/libmodule-pulseaudio-effects-sink.so ${D}/${libdir}/pulse-12.2/modules/module-pulseaudio-effects-sink.so
    rm ${D}/${libdir}/libmodule-pulseaudio-effects-sink.so
}


FILES_SOLIBSDEV = ""
FILES_${PN} += "${libdir}/pulse-12.2/modules/*.so"
FILES_${PN} += "${libdir}/*.so"
