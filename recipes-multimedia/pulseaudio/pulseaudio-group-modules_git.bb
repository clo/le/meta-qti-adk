SUMMARY = "PulseAudio Group Modules"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = " \
    file://modules/module_group_manager.cc;beginline=8;endline=19;md5=d0bd90f3f6928bd591e708954b3f5fe1 \
    file://modules/pulsecore_config.h;beginline=4;endline=16;md5=4ce698c728a5da055b5ee727fad048ae \
    "
FILESPATH_prepend = "${WORKSPACE}/external/pulseaudio/src/modules/:"

SRC_URI = "\
    file://group-modules/ \
    "

SRC_DIR = "${WORKSPACE}/external/pulseaudio/src/modules/group-modules"
S = "${WORKDIR}/group-modules"

DEPENDS = "pulseaudio adk-config adk-message-service"
RDEPENDS_${PN} += "pulseaudio-server adk-message-service"

EXTRA_OECMAKE += "-DCMAKE_SYSROOT=${RECIPE_SYSROOT}"

inherit cmake pkgconfig

PACKAGES += "pulseaudio-group-null"

FILES_${PN} += "\
    ${libdir}/pulse*/modules/module*.so \
    "
FILES_pulseaudio-group-null = "\
    ${libdir}/pulse*/modules/libpulse*.so \
    "
