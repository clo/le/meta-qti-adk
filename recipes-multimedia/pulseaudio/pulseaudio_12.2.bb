require pulseaudio.inc

FILESPATH =+ "${WORKSPACE}/:"
SRC_URI = "file://external/pulseaudio/ \
           file://volatiles.04_pulse \
           file://pulseaudio.service \
           file://volatile-var-lib-pulse.service \
           file://system-${BASEMACHINE}.pa \
           file://pa_config_update \
           file://pa_config.d \
           file://pulse.conf \
"

S = "${WORKDIR}/external/pulseaudio"

PULSE_CONFIG_DIR = "/etc/pulse/pa_config.d"

do_configure_prepend() {
    cd ${S}
    NOCONFIGURE=1 ./bootstrap.sh
    cd ${B}
}

do_compile_prepend() {
    mkdir -p ${S}/libltdl
    cp ${STAGING_LIBDIR}/libltdl* ${S}/libltdl
}

do_install_append() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/pa_config_update ${D}${bindir}/pa_config_update
    install -d ${D}${PULSE_CONFIG_DIR}/
    install -m 0644 ${WORKDIR}/pa_config.d/* ${D}${PULSE_CONFIG_DIR}/

    install -d ${D}/${sysconfdir}/dbus-1/session.d/
    install -m 0644 ${WORKDIR}/pulse.conf ${D}${sysconfdir}/dbus-1/session.d/
}

FILES_${PN}-server += "\
    ${bindir}/pa_config_update \
    ${PULSE_CONFIG_DIR}/ \
    "
