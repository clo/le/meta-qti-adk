#!/bin/sh

ADK_CONFIG=/data/adk.audiomanager.db

# Need to re-visit the need of normal mode. Since there is no "normal
# latency" sink with TTP support (no "offload0-ttp" sink), we alway use
# the low-latency mode.
#
#latency_level="$(adkcfg -f $ADK_CONFIG read audio.manager.latency_level 2>/dev/null)"
#if [ "$latency_level" = "Normal" ]
#then
#	main_sink="offload0"
#else
#	main_sink="offload_ll"
#fi

main_sink="offload_ll"

# Pre-existing sinks
if [ $timestamp_mode == "true" ]; then
	create_extern_sink offload_ll "offload-low-latency-ttp"
else
	create_extern_sink offload_ll "offload-low-latency"
fi
create_extern_sink offload0

# BT sink proxy/placeholder
create_sink bt_proxy "module-null-sink"
set_sink_param bt_proxy "channels" "2"
set_sink_param bt_proxy "channel_map" "front-left,front-right"
set_sink_param bt_proxy "timestamp_mode" "$timestamp_mode"

# BT sink remap (anchor point)
create_sink bt_proxy_remap "module-remap-sink"
set_sink_child bt_proxy_remap "master" bt_proxy
set_sink_param bt_proxy_remap "remix" "no"
set_sink_param bt_proxy_remap "channels" "2"
set_sink_param bt_proxy_remap "master_channel_map" "front-left,front-right"
set_sink_param bt_proxy_remap "channel_map" "front-left,front-right"

# HDMI Tx sink proxy/placeholder
create_sink hdmi_proxy "module-null-sink"
set_sink_param hdmi_proxy "channels" "2"
set_sink_param hdmi_proxy "channel_map" "front-left,front-right"
set_sink_param hdmi_proxy "timestamp_mode" "$timestamp_mode"

# HDMI sink remap (anchor point)
create_sink hdmi_proxy_remap "module-remap-sink"
set_sink_child hdmi_proxy_remap "master" hdmi_proxy
set_sink_param hdmi_proxy_remap "remix" "no"
set_sink_param hdmi_proxy_remap "channels" "2"
set_sink_param hdmi_proxy_remap "master_channel_map" "front-left,front-right"
set_sink_param hdmi_proxy_remap "channel_map" "front-left,front-right"

# Combine sink for stereo (downmixed) audio in case it is needed later
# by other helper scripts
create_sink stereo_downmix "module-combine-sink"
set_sink_auto stereo_downmix
set_sink_child stereo_downmix "slaves" bt_proxy_remap
add_sink_child stereo_downmix hdmi_proxy_remap
set_sink_param stereo_downmix "channels" "2"
set_sink_param stereo_downmix "channel_map" "front-left,front-right"
[ $timestamp_mode == "true" ] && set_sink_param stereo_downmix "adjust_time" "0"

# Local playback remap sink
create_sink user_config_remap "module-remap-sink"
set_sink_child user_config_remap "master" "$main_sink"

# Global combine sink (splitter_music)
create_sink splitter_music "module-combine-sink"
set_sink_child splitter_music "slaves" user_config_remap
add_sink_child splitter_music stereo_downmix
[ $timestamp_mode == "true" ] && set_sink_param splitter_music "adjust_time" "0"

# Add group-manager
if [ $timestamp_mode == "true" ]; then
	create_sink group_manager "module-group-manager"
	set_sink_name group_manager "" "group_manager" # name constructed by sink, so don't set a name for that PA parameter
	set_sink_child group_manager "master" splitter_music

	set_default_sink group_manager
else
	set_default_sink splitter_music
fi
